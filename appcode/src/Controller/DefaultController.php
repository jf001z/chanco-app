<?php
/**
 * Created by PhpStorm.
 * User: jeffzhang
 * Date: 23/03/2018
 * Time: 20:34
 */

namespace App\Controller;


use Chancolib\ArticleService\ArticleService;
use Chancolib\Cache\Cache;
use Chancolib\ImageService\ImageService;
use Chancolib\Queue\BeanstalkClient;
use Chancolib\Queue\JobService;
use Chancolib\Queue\QueueName;
use Doctrine\ORM\Configuration;
use function PHPSTORM_META\type;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Chancolib\AwsService\AwsS3Client;

//use Chancolib\Model;


class DefaultController extends Controller
{
    public function indexAction(Request $request){
        //$em = $this->getDoctrine()->getManager();
        //$sources = $em->getRepository('ChancoEntity:Source')->getAllSourcesCodes();
        //var_dump($sources);
        //$info = json_encode($em->getRepository('ChancoEntity:GlobalSetting')->getSetting());

        //$this->get('beanstalkd_service');
        //$jobService = new JobService();
        //$jobService->createJob('this is a test',QueueName::ARTICLE_GET_CONTENT);
        //$articleService = new ArticleService($this->get('service_container'));
        //var_dump($articleService->getAllArticles());
        /*$info = '';
        //return new Response("<html><body>$info</body></body></html>");
        if(!empty($product) && is_array($product)){
            foreach ($product as $key=>$d){
                if($key != 'property'){
                    if(!gettype($d) == 'object'){
                        $info .= '<p>'.$key . ' : '. $d.'</p>';
                    }
                }else{
                    foreach ($product['property'] as $p=>$pv){
                        $info .= '<p>'.$p. ' : '. $pv['id'].', '.$pv['value'].'</p>';
                    }
                }
            }
        }else{
            $info = $product;
        }

        //var_dump($product->getProductPropertyAttributes());
        //var_dump($ppa);
        */


        $id = $request->getSession()->getId();
        $request->getSession()->set($id,'this is a session test');

        $response = new Response($this->renderView('index.html.twig',
            array('title'=>'default',
                'info'=>$id,
                'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR)),
            200,array());

        $response->headers->setCookie(new Cookie('PSESSIONID',$id,time() + 1200));
        return $response;
    }

    public function portfolioAction(Request $request){
        $response = new Response($this->renderView('portfolio.html.twig',
            array('title'=>'portfolio',
                'info'=>'portfolio',
                'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR)),
            200,array());
        return $response;
    }
    public function testAction(Request $request){

        $sessionKey = '';
        if($request->cookies->has('PSESSIONID')){
            $sessionKey = $request->cookies->get('PSESSIONID');
        }

        if(empty($sessionKey)){
            $response = new Response($this->renderView('index.html.twig',
                array('title'=>'default','info'=>'Session Key is not in Cookie')),200,array());
        }else{
            if($request->getSession()->has($sessionKey)){
                $message = $request->getSession()->get($sessionKey);
            }else{
                $message = 'session key is not in session';
            }
            $response = new Response($this->renderView('index.html.twig',
                array('title'=>'default','info'=>$message)),200,array());
        }

        return $response;
    }
    public function dataAction()
    {
        return new JsonResponse([
            [
                'id' => 1,
                'author' => 'Chris Colborne',
                'avatarUrl' => 'http://1.gravatar.com/avatar/13dbc56733c2cc66fbc698cdb07fec12',
                'title' => 'Bitter Predation',
                'description' => 'Thirteen thin, round towers …',
            ],
            [
                'id' => 2,
                'author' => 'Louanne Perez',
                'avatarUrl' => 'https://randomuser.me/api/portraits/thumb/women/18.jpg',
                'title' => 'Strangers of the Ambitious',
                'description' => "A huge gate with thick metal doors …",
            ],
            [
                'id' => 3,
                'author' => 'Theodorus Dietvorst',
                'avatarUrl' => 'https://randomuser.me/api/portraits/thumb/men/49.jpg',
                'title' => 'Outsiders of the Mysterious',
                'description' => "Plain fields of a type of grass cover …",
            ],
        ]);
    }
}