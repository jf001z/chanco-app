<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 02/08/2018
 * Time: 15:59
 */

namespace App\Controller\Api;


use Chancolib\ArticleService\ArticleService;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ApiController extends FOSRestController
{
    /**
     * @Route("/getSourceList",name="SourceList")
     * @Method({"GET", "POST"})
     */
    public function getSourceListAction(Request $request){
        $source = new ArticleService($this->get('service_container'));
        return new JsonResponse($source->getAllSourceInCache());
    }
    /**
     * @Route("/getAllArticles",name="allArticles")
     * @Method({"GET"})
     */
    public function getAllArticles(Request $request){
        $articleService = new ArticleService($this->get('service_container'));
        $articles = $articleService->getAllArticles();
        return new JsonResponse($articles);
    }
}