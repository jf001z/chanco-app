<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 22/04/2018
 * Time: 11:59
 */

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Chancolib\Config\Configuration;
use Symfony\Component\Cache\Simple\ArrayCache;

class ChancoConfigurator
{
    private $config;

    public function __construct($config)
    {

        $this->config = $config;
        Configuration::configurate($this->config);

    }

    public function onKernelRequest(GetResponseEvent $event){

    }

}