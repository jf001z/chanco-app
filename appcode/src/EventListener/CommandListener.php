<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 29/05/2018
 * Time: 07:36
 */

namespace App\EventListener;

use Chancolib\Config\Configuration;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\ConsoleEvents;

class CommandListener
{
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function onConsoleCommand(ConsoleCommandEvent $event) {
        // get the output instance
        $output = $event->getOutput();

        // get the command to be executed
        $command = $event->getCommand();
        Configuration::configurate($this->config);
        // write something about the command
        //$output->writeln(Configuration::get('beanstalk_info'));
        $output->writeln(sprintf('Before running command <info>%s</info>', $command->getName()));
    }
}