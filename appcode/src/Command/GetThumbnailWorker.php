<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/06/2018
 * Time: 23:59
 */

namespace App\Command;


use Chancolib\ArticleService\ArticleService;
use Chancolib\ImageService\ImageService;
use Chancolib\Queue\JobService;
use Chancolib\Queue\QueueName;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetThumbnailWorker extends ContainerAwareCommand
{
    private $logger;
    protected function configure()
    {
        $this
            ->setName('news:get-article-thumbnail')
            ->setDescription('Get Image and Thumbnail to API Gateway')
            ->addOption('all','a',InputOption::VALUE_NONE,'Get content for all not processed articles');
    }
    public function setLogger(LoggerInterface $logger){
        $this->logger = $logger;
    }
    protected function execute(InputInterface $input, OutputInterface $output){

        $articleService = new ArticleService($this->getContainer());
        $imageService = new ImageService();
        $path = "/var/www/html/public/temp/";


        if($input->getOption('all')){
            $articles = $articleService->get10UnprocessedImageArticles();
            $output->writeln('There are '.count($articles).' Unprocessed articles');
            if(is_null($articles) || empty($articles)){
                $output->writeln("There is no unprocessed image articles.");
                return;
            }
            foreach ($articles as $article){
                $image_url = $article->getImageUrl();
                if(empty($image_url)){
                    $output->writeln("There is no image for this article.");
                    continue;
                }

                $new_image_name = Uuid::uuid1().'.jpg';
                $full_path = $path.$new_image_name;

                $imageService->downaloadImage($image_url,$full_path);

                $image_s3_url = $imageService->uploadImageToS3($full_path);

                $output->writeln($image_s3_url);

                $thumbnail_url = '';
                if(!empty($imageService->getFileS3Key())){
                    $thumbnail_url = $imageService->getImageTumbnail();
                }
                $output->writeln($thumbnail_url);

                $articleService->updateArticleS3ImgThumbnail($article->getArticleId(), $image_s3_url, $thumbnail_url);
            }
        }else{
            $jobService = new JobService();
            $job = $jobService->getJob(QueueName::ARTICLE_GET_THUMBNAIL);

            $articleId = $job->getData();

            if(empty($articleId)){
                $output->writeln("The aricleId is empty.");
                return;
            }
            if(!is_numeric($articleId)){
                $jobService->deleteJob($job);
                $this->logger->error('This is not an article Id: '. $articleId);
                return;
            }


            $image_url = $articleService->getImageUrlById($articleId);
            if(empty($image_url)){
                $jobService->deleteJob($job);
                $output->writeln("There is no image for this article.");
                return;
            }

            $new_image_name = Uuid::uuid1().'.jpg';
            $full_path = $path.$new_image_name;

            $imageService->downaloadImage($image_url,$full_path);
            $image_s3_url = $imageService->uploadImageToS3($full_path);

            $thumbnail_url = '';
            if(!empty($imageService->getFileS3Key())){
                $thumbnail_url = $imageService->getImageTumbnail();
            }

            $articleService->updateArticleS3ImgThumbnail($articleId, $image_s3_url, $thumbnail_url);

            $output->writeln('Thumbnail link is '.$thumbnail_url);
            //$this->logger->info('Content ID is: '.$contentId.'. Article Id is '.$articleId);
            $jobService->deleteJob($job);

        }

    }
}