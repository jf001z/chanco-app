<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 26/05/2018
 * Time: 19:41
 */

namespace App\Command;


use Chancolib\Entity\Article;
use Chancolib\Entity\Content;
use Chancolib\Queue\JobService;
use Chancolib\Queue\QueueName;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class NewsDownloadCommand extends ContainerAwareCommand
{
    private $logger;
    public function setLogger(LoggerInterface $logger){
        $this->logger = $logger;
    }
    protected function configure()
    {
        $this
            ->setName('news:download')
            ->setDescription('Get News Resource from News API periodically');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        if($container->hasParameter('chanco')){
            $config = $container->getParameter('chanco');
            $baseURL = $config['news_api_info']['base_url'];
            $apiKey = $config['news_api_info']['news_api_key'];
            $em = $container->get('doctrine')->getEntityManager();
            $newsSources  = $em->getRepository('ChancoEntity:Source')->getAllSourcesCodes();
            //$output->writeln($baseURL.$apiKey);
            $articleEm = $em->getRepository('ChancoEntity:Article');
            foreach ($newsSources as $r) {
                $output->writeln('Source id is '.$r['sourceId']);
                $output->writeln('Source code is '.$r['sourceCode']);
                $url = str_replace(':api_key', $apiKey, $baseURL);
                $url = str_replace(':source_string', $r['sourceCode'], $url);

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = null;
                $response = curl_exec($curl);
                if (empty($response)) {
                    $output->writeln("Cannot get date from News API.");
                    return;
                }
                $data = json_decode($response, true);
                if (!$data["status"] == 'ok') {
                    $output->writeln("Cannot get date from News API.");
                    return;
                }
                $articles = $data["articles"];
                curl_close($curl);
                if(is_null($r['sourceId'])){
                    var_dump($r);
                    die();
                }
                $contentJob = new JobService();
                foreach ($articles as $a) {

                    $article = new Article();
                    $article->setAuthor(is_null($a['author'])?'':$a['author']);
                    $article->setUrl($a['url']);
                    $article->setTitle($a['title']);
                    empty($a['urlToImage'])?$article->setImageUrl(''):$article->setImageUrl($a['urlToImage']);
                    $article->setSourceId($r['sourceId']);
                    empty($a['description'])?$article->setDescription(''):$article->setDescription($a['description']);
                    $article->setProcessed(0);
                    $t = new \DateTime($articleEm->dateFormatTransform($a['publishedAt']));
                    $article->setPublishAt($t);
                    //$output->writeln($article->getPublishAt())

                    if(!$articleEm->isOldArticle($article)){
                        $em->persist($article);
                        $em->flush();
                        $article_id = $article->getArticleId();
                        $output->writeln('Article Id is: '.$article_id);
                        //create get content beanstalk job.
                        $contentJob->createJob($article_id, QueueName::ARTICLE_GET_CONTENT);
                        $contentJob->createJob($article_id, QueueName::ARTICLE_GET_THUMBNAIL);

                    }else{
                        $output->writeln($a['title'].' is already in the database');
                    }

                }
            }


        }
    }

}