<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 03/07/2018
 * Time: 23:30
 */

namespace App\Command;


use Chancolib\Queue\JobService;
use Chancolib\Queue\QueueName;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetKeywordWorker extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('news:get-article-keyword')
            ->setDescription('Get article Keyword');
    }
    protected function execute(InputInterface $input, OutputInterface $output){
        $jobService = new JobService();
        $job = $jobService->getJob(QueueName::ARTICLE_GET_KEYWORDS);
        $contentId = $job->getData();

        if(empty($contentId)){
            $output->writeln("The ContentId is empty.");
            return;
        }
    }

}