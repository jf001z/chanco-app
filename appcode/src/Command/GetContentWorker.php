<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 27/06/2018
 * Time: 23:59
 */

namespace App\Command;


use Chancolib\ArticleService\ArticleService;
use Chancolib\Queue\JobService;
use Chancolib\Queue\QueueName;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GetContentWorker extends ContainerAwareCommand
{
    private $logger;
    protected function configure()
    {
        $this
            ->setName('news:get-article-content')
            ->setDescription('Get article content')
            ->addOption('all','a',InputOption::VALUE_NONE,'Get content for all not processed articles');
    }
    public function setLogger(LoggerInterface $logger){
        $this->logger = $logger;
    }
    protected function execute(InputInterface $input, OutputInterface $output){

        $articleService = new ArticleService($this->getContainer());

        if($input->getOption('all')){
            $articles = $articleService->getAllUnprocessedArtiles();
            $output->writeln('There are '.count($articles).' Unprocessed articles');
            if(is_null($articles) || empty($articles)){
                $output->writeln("There is no unprocessed articles.");
                return;
            }
            foreach ($articles as $article){
                $contentId = $articleService->getContentByArticle($article);
                $output->writeln('Content ID is: '.$contentId.'. Article Id is '.$article->getArticleId());
                $this->logger->info('Content ID is: '.$contentId.'. Article Id is '.$article->getArticleId());
            }
        }else{
            $jobService = new JobService();
            $job = $jobService->getJob(QueueName::ARTICLE_GET_CONTENT);

            $articleId = $job->getData();

            if(empty($articleId)){
                $output->writeln("The aricleId is empty.");
                return;
            }
            if(!is_numeric($articleId)){
                $jobService->deleteJob($job);
                $this->logger->error('This is not an article Id: '. $articleId);
                return;
            }

            $contentId = $articleService->getContentById($articleId);
            $output->writeln('Content ID is: '.$contentId.'. Article Id is '.$articleId);
            //$this->logger->info('Content ID is: '.$contentId.'. Article Id is '.$articleId);
            if($contentId){
                $jobService->deleteJob($job);
                //$jobService->createJob($contentId,QueueName::ARTICLE_GET_KEYWORDS);
            }else{
                $jobService->bureyJob($job);
                $this->logger->info('Article '.$articleId.' has already ');
            }
        }

    }
}