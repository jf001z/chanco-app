import React from 'react';
import ReactDOM from 'react-dom';
import GridLayout from './Components/GridLayout';
import "typeface-roboto"

class App extends React.Component {

    render() {
        return (
            <div>
                <GridLayout/>
            </div>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));