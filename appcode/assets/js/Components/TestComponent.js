import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ItemCard from './ItemCard';
import {withStyles} from "@material-ui/core/styles/index";

class TestComponent extends React.Component {
    constructor() {
        super();
        this.state = {
            entries: []
        };
    }

    componentDidMount() {
        fetch('/data')
            .then(response => response.json())
            .then(entries => {
                this.setState({
                    entries
                });
            });
    }

    render() {
        return (
            <div>
                <MuiThemeProvider>
                    <div style={{ display: 'flex' }}>
                        {this.state.entries.map(
                            ({ id, author, avatarUrl, title, description }) => (
                                <ItemCard
                                    key={id}
                                    author={author}
                                    title={title}
                                    avatarUrl={avatarUrl}
                                    style={{ flex: 1, margin: 10 }}
                                >
                                    {description}
                                </ItemCard>
                            )
                        )}
                    </div>
                </MuiThemeProvider>
            </div>
        );
    }
}


export default TestComponent;
