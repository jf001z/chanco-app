import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';


const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
});

class MenuAppBar extends React.Component {
    constructor(props){
        super();
        this.state = {
            auth: true,
            anchorEl: null,
            value: 0,
            sourceNames:[],
            classes: props
        };
    }

    componentDidMount(){
        fetch('/getSourceList')
            .then(response => response.json())
            .then(sourceNames => {
                this.setState(
                { sourceNames: sourceNames }
                );
                {/*
                sourceNames.map((s,i) => {
                    console.log(s.sourceId);
                    console.log(i);
                    console.log(s.sourceName);
                })
                */}

            });
    }

    handleChange = (event, value) => {
        this.setState({ value });
        this.props.updateSourceCode(value);
        console.log(value);
        //console.log(this.props);
    };

    handleMenu = event => {
        if(this.state.anchorEl == null){
            this.setState({ anchorEl: event.currentTarget });
        }else {
            this.setState({ anchorEl: null });
        }

    };
    handleTab = (id,sourceCode) => {
        this.handleClose();
    }

    handleClose = () => {
        this.setState({ anchorEl: null });
    }
    handleRedirect = (link) => {
        window.location = link;
    }
    render() {
        const { classes } = this.props;
        const { anchorEl, value, sourceNames } = this.state;
        const open = Boolean(anchorEl);
        return (
            <div className={classes.root}>
                <AppBar position="fixed">
                    <Toolbar>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            News Resources
                        </Typography>
                            <div>
                                <IconButton
                                    aria-owns={open ? 'menu-appbar' : null}
                                    aria-haspopup="true"
                                    onClick={()=>this.handleRedirect('/')}
                                    color="inherit"
                                >
                                    <MenuIcon />
                                </IconButton>
                                <IconButton
                                    aria-owns={open ? 'menu-appbar' : null}
                                    aria-haspopup="true"
                                    color="inherit"
                                    onClick={()=>this.handleRedirect('/portfolio')}
                                >
                                    <AccountCircle />
                                </IconButton>

                                {/*
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                    <MenuItem onClick={this.handleClose}>My account</MenuItem>
                                </Menu>
                                */}
                            </div>
                    </Toolbar>
                    {
    open && <div className={classes.root}>
        <AppBar position="static"
                color="default"
        >
            <Tabs
                value={value}
                onChange={this.handleChange}
                indicatorColor="primary"
                textColor="primary"
                scrollable
                scrollButtons="auto"
            >
                <Tab onClick={()=>this.handleTab(0,'all')} tabIndex='0' key='0' value='all' label='ALL'/>
                {
                    sourceNames.map((s,index) =>
                    <Tab
                    onClick={() => this.handleTab(s.sourceId,s.sourceCode)}
                    value={s.sourceCode}
                    key = {(index + 1).toString()}
                    tabIndex={s.sourceId}
                    label={s.sourceName} />
                    )}
            </Tabs>
        </AppBar>
    </div>
}
                </AppBar>
            </div>
        );
    }
}

MenuAppBar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuAppBar);
