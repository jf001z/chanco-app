import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import CameraIcon from '@material-ui/icons/PhotoCamera';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
    icon: {
        marginRight: theme.spacing.unit * 2,
    },
    heroUnit: {
        backgroundColor: theme.palette.background.paper,
    },
    heroContent: {
        maxWidth: 600,
        margin: '0 auto',
        padding: `${theme.spacing.unit * 8}px 0 ${theme.spacing.unit * 6}px`,
    },
    heroButtons: {
        marginTop: theme.spacing.unit * 4,
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
            width: 1100,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    cardGrid: {
        padding: `${theme.spacing.unit * 8}px 0`,
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '66%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
    noMaxWidth: {
        maxWidth: 'none',
    }
});


class ArticlesComponent extends React.Component {
    constructor(props) {
        super();
        this.state = {
            entries: [],
            allArtcles:[],
            classes: props
        };
    }
    componentDidMount() {
        fetch('/getAllArticles')
            .then(response => response.json())
            .then(entries => {
                this.setState({
                    entries:entries,
                    allArtcles:entries
                });
            });
    }
    componentWillReceiveProps(nextProps) {
        console.log(nextProps.currentSourceCode);
        var temp = {};
        if(nextProps.currentSourceCode == 'all'){
            temp = this.state.allArtcles;
        }else{
            temp[nextProps.currentSourceCode] = this.state.allArtcles[nextProps.currentSourceCode];
        }
        this.setState({
            entries:temp
        });
    }
    render() {
        const { entries } = this.state;
        const { classes } = this.state.classes;
        return (
            <div className={classNames(classes.layout, classes.cardGrid)} >
                <Grid container spacing={40}>
                    {Object.keys(entries).map( (channel_code,key) => (
                            entries[channel_code].map( article => (

                                    <Grid item key={article.articleId} sm={6} md={4} lg={3}>
                                        <Card className={classes.card}>
                                            <CardMedia
                                                className={classes.cardMedia}
                                                image={article.thumbnailUrl.length>0?article.thumbnailUrl.replace(/\"/g,""):article.imageUrl}
                                            />
                                            <CardContent className={classes.cardContent}>
                                                <Tooltip title={article.title} classes={{ tooltip: classes.noMaxWidth }}>
                                                    <Typography gutterBottom variant="title">
                                                        {article.title.split(' ').slice(0,4).join(' ') + ' ...'}
                                                    </Typography>
                                                </Tooltip>
                                                <Typography>
                                                    {article.description.split(' ').slice(0,12).join(' ') + ' ...'}
                                                </Typography>
                                            </CardContent>
                                            <CardActions>
                                                <Button size="small" color="primary">
                                                    View
                                                </Button>
                                                <Button size="small" color="primary">
                                                    Edit
                                                </Button>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                )
                            )
                        )
                    )}
                </Grid>
            </div>
        );
    }
}

ArticlesComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ArticlesComponent);
