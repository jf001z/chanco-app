import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import MenuAppBar from './MenuAppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import ArticlesComponent from './ArticlesComponent'
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing.unit * 6,
    },
});

class GridLayout extends React.Component{

    constructor(props){
        super();
        this.state = {
            classes: props,
            sourceCode: 'all'
        };
    }
    updateSourceCode = (sourceCode) =>{
        this.setState({
            sourceCode:sourceCode
        });
        console.log('Selected code is: ' + sourceCode);
    }
    render(){
        const { classes } = this.state.classes;
        return (
            <React.Fragment>
                <CssBaseline />
                <MenuAppBar updateSourceCode = {this.updateSourceCode} />
                <Grid container spacing={40}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}></Paper>
                    </Grid>
                </Grid>
                <Grid container spacing={40}>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}></Paper>
                    </Grid>
                </Grid>
                <main>
                    <ArticlesComponent currentSourceCode = {this.state.sourceCode}/>
                </main>
                <footer className={classes.footer}>
                    <Typography variant="title" align="center" gutterBottom>
                        Footer
                    </Typography>
                    <Typography variant="subheading" align="center" color="textSecondary" component="p">
                        Something here to give the footer a purpose!
                    </Typography>
                </footer>
                {/* End footer */}
            </React.Fragment>
        );
    }

}

GridLayout.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GridLayout);
