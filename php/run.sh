#!/bin/bash

# Install Composer Dependencies
if [ $COMPOSER_INSTALL = "true" ]; then
    composer install --no-interaction --verbose
fi

# Copy parameters Distribution file to parameters.yml
cp /var/www/html/app/config/parameters.yml.dist /var/www/html/app/config/parameters.yml

#if [ $BOWER_INSTALL = "true" ]; then
    echo "Building ces.js"

    cd '/var/www/html/jssrc/ces/' \
    && npm install \
    && node_modules/webpack/bin/webpack.js
#fi


if [ "$CSS_THEME" ]; then
    cd '/var/www/html/web/scss' \
    && sass --update --sourcemap=none --no-cache --style compressed theme/$CSS_THEME/:../css/$CSS_THEME
fi

if [ "$REQUEST_BASKET_CSS" = 'true' ]; then
    curl --data "applicationCode=SKUB$ORDER_PREFIX&name=$CSS_THEME&overrideScssUrl=$CANONICAL_BASE_URL/scss/theme/$CSS_THEME/_variables-override.scss" https://$BASKET_MICROSERVICE_HOST/scss/compile
fi

if [ "$NEWRELIC_LICENSE" ]; then
    sed -i "s/newrelic.license = \"REPLACE_WITH_REAL_KEY\"/newrelic.license = \"$NEWRELIC_LICENSE\"/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

if [ "$NEWRELIC_APPNAME" ]; then
    sed -i "s/newrelic.appname = \"PHP Application\"/newrelic.appname = \"$NEWRELIC_APPNAME\"/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

if [ "$NEWRELIC_ENABLED" ]; then
sed -i "s/;newrelic.enabled = true/newrelic.enabled = $NEWRELIC_ENABLED/" "/usr/local/etc/php/conf.d/newrelic.ini"
fi

sed -i "s/;newrelic.framework = \"\"/newrelic.framework = \"symfony\"/" "/usr/local/etc/php/conf.d/newrelic.ini"

if [ "$PHP_INI" ]; then
    mv /usr/local/etc/php/config/php-$PHP_INI.ini /usr/local/etc/php/php.ini
fi

if [ $ENABLE_OPCACHE = "true" ]; then
    sed -i "s/opcache.enable=0/opcache.enable=1/" "/usr/local/etc/php/conf.d/opcache.ini"
fi

if [ $ENABLE_XDEBUG = "true" ]; then
    mv /usr/local/etc/php/config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini
fi

if [ "$XDEBUG_IDEKEY" ]; then
    sed -i "s/;xdebug.idekey=/xdebug.idekey=$XDEBUG_IDEKEY/" "/usr/local/etc/php/conf.d/xdebug.ini"
fi

if [ "$XDEBUG_REMOTEHOST" ]; then
    sed -i "s/xdebug.remote_host=localhost/xdebug.remote_host=$XDEBUG_REMOTEHOST/" "/usr/local/etc/php/conf.d/xdebug.ini"
fi

# Run the console app:generate-asset-manifest

echo "Generate asset manifest"
echo "Waiting 30s for Container Network to be established...."
# Give MySql Service a chance to start
sleep 30
/usr/local/bin/php /var/www/html/bin/console app:generate-asset-manifest -v --env=${APP_ENV:-dev}

# Directories setup and permissions
echo "Create log directory and setup permissions"
mkdir -p /var/log/app/monolog
chmod -R 777 /var/www/html/var/cache
chmod -R 777 /var/www/html/var/logs
chmod -R 777 /var/www/html/var/sessions
chmod -R 777 /var/log/app

# Run Migrations
/usr/local/bin/php /var/www/html/bin/console doctrine:migrations:migrate -v --env=${APP_ENV:-dev}

# Cron Environment Setup
chmod 644 /etc/crontab
crontab /etc/crontab
touch /var/log/cron.log
chmod 777 /var/log/cron.log
env >> /etc/environment
service cron start

# Start supervisord and services
/usr/bin/supervisord -c /etc/supervisord.conf