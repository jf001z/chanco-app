import json
import boto3
import uuid
import cStringIO
from PIL import Image,ImageOps
import os

s3 = boto3.client('s3')


def lambda_handler(event, context):
    bucket = 'upload-images-bucket-v64rleca837do'

    # Get the url of the post data
    key = event.get("path")

    # resp = s3.list_objects_v2(Bucket=bucket)
    # keys = []
    # for obj in resp['Contents']:
    #    keys.append(obj['Key'])
    # dic_event = json.loads(event)

    img_obj = s3.get_object(Bucket=bucket, Key=key)
    obj_body = img_obj['Body'].read()
    img = Image.open(BytesIO(obj_body))

    # Get the url of the generated url

    original_url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket,
            'Key': key
        }
    )
    urls = original_url.split('?')
    url = urls[0]
    # resp = dict(status="successs",message=list(img_obj.keys()))
    resp = dict(status="success", message=url)
    return resp
    # return img_obj['Body']


def image_resize():
    return 'hello'