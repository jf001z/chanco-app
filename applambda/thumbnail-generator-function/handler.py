import json
import boto3
import logging
from io import BytesIO
from io import StringIO
from PIL import Image, ImageOps
import os
import datetime
import requests
import uuid
from datalinklayer import DataLinkLayer

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.client('s3')
width = int(os.environ['THUMBNAIL_WIDTH'])
height = int(os.environ['THUMBNAIL_HIGHT'])
bucket = os.environ['BUCKET']


def python_thumbnail_generator(event, context ):
    db = DataLinkLayer()
    if "Records" in event:
        article_ids = json.loads(event['Records'][0]['body'])
        query_ids = ','.join(str(x) for x in article_ids)
        sql = "SELECT * FROM article WHERE article_id in (%s)" % (query_ids)
        ar_infos = db.fetchAll(sql)
        print(ar_infos)
        thumb_array = []
        for ar_info in ar_infos:
            temp_thumb = dict()
            img_url = ar_info['image_url']
            article_id = ar_info['article_id']
            if img_url is None or img_url == '':
                thumb_url = 'https://s3-eu-west-1.amazonaws.com/upload-images-bucket-v64rleca837do/news-logo.jpg'
                #updateThumbnailInDb(thumb_url, article_id, db)
                #return thumb_url
                temp_thumb = {'id': article_id,'url': thumb_url}
            else:
                r = requests.get(img_url)
                image = Image.open(BytesIO(r.content))
                extension = getExtension(img_url)
                key = pathGenerator(extension)
                thumbnail = image_to_thumbnail(image)
                print(str(thumbnail))
                thumbnail_key = new_filename(key)
                thumb_url = upload_to_s3(bucket, thumbnail, thumbnail_key, extension)
                temp_thumb = {'id': article_id, 'url': thumb_url}
            thumb_array.append(temp_thumb)
        updateThumbnailInDb(thumb_array, db)
        return thumb_array
    else:
        key = event.get("path")
        extension = getExtension(key)
        image = get_s3_image(bucket, key)
        thumbnail = image_to_thumbnail(image)
        
        print(str(thumbnail))

        thumbnail_key = new_filename(key)

        url = upload_to_s3(bucket, thumbnail, thumbnail_key, extension)

        return url

def updateThumbnailInDb(thumb_array, db):
    update_sql = "UPDATE article SET thumbnail_url = CASE article_id "
    last_sql = "("
    for thumb in thumb_array:
        update_sql += "WHEN %s THEN '%s' " % (thumb['id'], thumb['url'])
        last_sql += "%s, "%(thumb['id'])
    last_sql = last_sql.rstrip(', ') + ")"
    update_sql = update_sql + "ELSE thumbnail_url END WHERE article_id IN " + last_sql
    print(update_sql)
    #update_sql = "UPDATE article set thumbnail_url=%s WHERE article_id=%s"
    params = ()
    db.execute(update_sql, params)
    db.commit()
    db.close()

def get_s3_image(bucket, key):
    img_obj = s3.get_object(Bucket=bucket, Key=key)
    obj_body = img_obj['Body'].read()
    file = BytesIO(obj_body)
    image = Image.open(file)
    return image

def pathGenerator(extension):
    now = datetime.datetime.now()
    path = str(now.year) + '/' + str(now.month) + '/' + str(now.day) + '/' + str(uuid.uuid4().hex) + '.' + extension
    return path

def image_to_thumbnail(image):
    return ImageOps.fit(image, (width, height), Image.ANTIALIAS)


def new_filename(key):
    key_split = key.rsplit('.', 1)
    return key_split[0] + "-thumbnail." + key_split[1]

def getExtension(key):
    key_split = key.rsplit('.', 1)
    t = key_split[1]
    ext = t.split('?')
    if len(ext[0]) > 4:
        ext[0] = 'jpg'
    return ext[0]

def upload_to_s3(bucket, image, key, extension):

    if extension.lower()=='jpg' or extension.lower()=='jpeg':
        ext = 'JPEG'
        contentType = 'image/jpg'
    elif extension.lower()=='png':
        ext = 'PNG'
        contentType = 'image/png'
    else:
        ext = 'JPEG'
        contentType = 'image/jpg'

    out_thumbnail = BytesIO()
    image.convert('RGB').save(out_thumbnail, ext);
    out_thumbnail.seek(0)
    resp = s3.put_object(
        ACL='public-read',
        Body=out_thumbnail,
        Bucket=bucket,
        ContentType=contentType,
        Key=key
    )
    print(resp)
    url = '{}/{}/{}'.format(s3.meta.endpoint_url, bucket, key)
    return url