import boto3
import json
import logging
from esService import esService
from datalinklayer import DataLinkLayer
logger = logging.getLogger()
logger.setLevel(logging.INFO)

def es_create_new_records(event, context):
    es = esService()
    db = DataLinkLayer()
    #sql = "SELECT * FROM article WHERE in_es=0"

    #Insert to ES

    sql = "SELECT * FROM article WHERE in_es=0"
    articles = db.fetchAll(sql)
    for article in articles:
        content_sql = "SELECT * FROM content WHERE article_id=%s" % (article["article_id"])
        cont = db.fetchOne(content_sql)
        ar_json = {
            "id": article["article_id"],
            "source_id": article["source_id"],
            "title": article["title"],
            "description": article["description"],
            "url": article["url"],
            "image_url": article["image_url"],
            "publish_at": article["publish_at"],
            "thumbnail_url": article["thumbnail_url"],
            "content": cont["content"]
        }
        #print(ar_json)
        result = es.insertRecord(ar_json)
        print(result)
        update_sql = "UPDATE article SET in_es=1 WHERE article_id=%s"
        params = (article["article_id"])
        db.execute(update_sql, params)
    db.commit()
    db.close()

    """
    # search in ES
    
    query = {
        "match": {
            "title": {
                "query": "Mexico City",
                "auto_generate_synonyms_phrase_query": "true"
            }
        }
    }
    sort = {
        "publish_at": {
            "order": "desc"
        }
    }
    num_limit = 10
    results = es.search(query, num_limit, sort)
    for article in results["hits"]["hits"]:
        print(article['_source']['title'])
    

    # Delete in ES
    query = {
        "query": {
            "match": {
                'title': 'Mariachi gunmen kill three in Mexico City'
            }
        }
    }
    result = es.delete(query)
    """