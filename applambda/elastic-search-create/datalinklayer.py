import pymysql
import boto3
import os
import re
#from pprint import pprint
#from inspect import getmembers


class DataLinkLayer:
    dbClient = None
    dbConfig = None
    dbParam = None

    def __init__(self):
        self.dbParam = ['CHANCO_DB_HOST', 'CHANCO_DB_PASS', 'CHANCO_DB_USER']
        ssm_client = boto3.client('ssm')
        self.updateConfig("CHANCO_DB_HOST|CHANCO_DB_PASS|CHANCO_DB_USER", self.dbParam, ssm_client)
        self.dbClient = self.getDbClient(self.dbConfig)

    def getDbClient(self, config):
        conn = pymysql.connect(
            host=config['CHANCO_DB_HOST'],
            user=config['CHANCO_DB_USER'],
            password=config['CHANCO_DB_PASS'],
            db="chanco",
            connect_timeout=39)
        return conn

    def fetchOne(self, sql):
        result = None
        if self.dbClient is not None:
            with self.dbClient.cursor(pymysql.cursors.DictCursor) as cursor:
                cursor.execute(sql)
                result = cursor.fetchone()
                cursor.close()
        return result

    def fetchAll(self, sql):
        result = None
        if self.dbClient is not None:
            with self.dbClient.cursor(pymysql.cursors.DictCursor) as cursor:
                cursor.execute(sql)
                result = cursor.fetchall()
                cursor.close()
        return result

    def checkExist(self, sql, params):
        result = None
        if self.dbClient is not None:
            with self.dbClient.cursor() as cursor:
                cursor.execute(sql, params)
                result = cursor.fetchone()
                cursor.close()
        return result
    def getConfig(self, pattern, pramas, ssm_client):
        #pa = ssm_client.get_parameters(Names=pramas, WithDecryption=False)
        returnParam = {}
        for prama in pramas:
            returnParam[prama] = os.environ[prama]
        return returnParam

    def updateConfig(self, pattern, pramas, ssm_client):
        self.dbConfig = self.getConfig(pattern, pramas, ssm_client)

    def execute(self, sql, parameters):
        """
        Execute Sql command
        """
        with self.dbClient.cursor() as cursor:
            cursor.execute(sql, parameters)
            lastRowId = cursor.lastrowid
            cursor.close()
            return lastRowId

    def commit(self):
        self.dbClient.commit()

    def close(self):
        self.dbClient.close()
