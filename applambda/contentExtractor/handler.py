import json
import boto3
from datalinklayer import DataLinkLayer
import logging
from newspaper import Article

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def content_extractor(event, context):
    db = DataLinkLayer()
    """
    sqs = boto3.resource('sqs')
    content_queue = sqs.get_queue_by_name(QueueName='getContent')
    msg = content_queue.receive_messages(
        MaxNumberOfMessages=1,
        VisibilityTimeout=3,
        WaitTimeSeconds=7,
        ReceiveRequestAttemptId='string'
    )
    """
    #for m in msg:
    #article_id = m.body
    #article_id = 1204
    article_id = event['Records'][0]['body']

    sql = "SELECT * FROM article WHERE article_id=%s" % (article_id)
    ar_info = db.fetchOne(sql)
    url = ar_info['url']
    article = Article(url)
    article.download()
    article.parse()
    text = article.text
    ins_sql = "INSERT INTO content (`article_id`, `content`) VALUES (%s,%s);"
    params = (article_id, text)
    content_id = db.execute(ins_sql, params)
    logger.info('content Id is '+str(content_id))
    update_sql = "UPDATE article SET processed=1 WHERE article_id=%s"
    update_params = (article_id)
    db.execute(update_sql, update_params)
    db.commit()
    db.close()


