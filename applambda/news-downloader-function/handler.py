import boto3
from datalinklayer import DataLinkLayer
import json
import requests
import logging

logger = logging.getLogger()
logger.setLevel(logging.WARNING)
def articleDownloader(event, context):
    net_pramas = ['NEWS_API_LINK','NEWS_API_KEY']
    db = DataLinkLayer()
    data = db.fetchAll('select * from source')
    news_params = db.getConfig("NEWS_API_LINK|NEWS_API_KEY", ['NEWS_API_LINK', 'NEWS_API_KEY'], boto3.client('ssm'))
    sqs = boto3.resource('sqs')
    content_queue = sqs.get_queue_by_name(QueueName='getContent')
    #thumbnail_queue = sqs.get_queue_by_name(QueueName='getThumbnail')
    for d in data:
        news_url = news_params['NEWS_API_LINK'] % (d['source_code'], news_params['NEWS_API_KEY'])
        r = None
        r = requests.get(news_url)
        if r.status_code == 200:
            data = r.json()
            if data['status']!= 'ok':
                logger.error("Cannot get date from News API.")
                return
            articles = data["articles"]
            for article in articles:
                sql = "SELECT 1 FROM `article` WHERE `title`=%s AND `source_id`=%s"
                parameters = (article['title'], d['source_id'])
                ar = db.checkExist(sql, parameters)
                if ar is None:
                    sql = """INSERT INTO article (`title`,`author`,`source_id`,`description`,`url`,`image_url`,`publish_at`,`processed`,`image_s3_url`,`thumbnail_url`) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,null,null);"""
                    if article['author'] is None:
                        author = ''
                    else:
                        author = article['author']
                    if article['urlToImage'] is None:
                        imageUrl = ''
                    else:
                        imageUrl = article['urlToImage']
                    if article['description'] is None:
                        description = ''
                    else:
                        description = article['description']

                    parameters = (article['title'], author, d['source_id'], description, article['url'], imageUrl, article['publishedAt'], 0)
                    id = db.execute(sql, parameters)
                    q_msg = str(id)
                    r = content_queue.send_message(MessageBody = q_msg)
                    logger.info(r)
                    #r_img = thumbnail_queue.send_message(MessageBody = q_msg)
                    #logger.info(r_img)
                else:
                    logger.warning('the article is already in the db')
            db.commit()
    db.close()
