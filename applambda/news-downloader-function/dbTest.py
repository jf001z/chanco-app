import pymysql
import boto3
import re


def lambda_handler(event, context):
    ssm_client = boto3.client('ssm')
    pramas = configParam()
    test = getConfig(pramas, ssm_client)
    print(test)
    """
    conn = pymysql.connect(params['CHANCO_DB_HOST'],user=params['CHANCO_DB_USER'],password=params['CHANCO_DB_PASS'],db="chanco", connect_timeout=39)
    with conn.cursor() as cur:
        cur.execute("select * from source")
        for row in cur:
            print(row)
    """


def configParam():
    return['CHANCO_DB_HOST', 'CHANCO_DB_PASS', 'CHANCO_DB_USER']


def getConfig(pramas, ssm_client):
    pattern = "CHANCO_DB_HOST|CHANCO_DB_PASS|CHANCO_DB_USER"
    pa = ssm_client.get_parameters(Names=pramas, WithDecryption=False)
    returnParam = {}
    for data in pa['Parameters']:
        key = re.search(pattern,data['Name'])
        if(key): returnParam[key] = data['Value']
    return returnParam