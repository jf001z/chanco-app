import json
import boto3
from datalinklayer import DataLinkLayer
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def thumbnail_queue_trigger(event, context):

    db = DataLinkLayer()
    sqs = boto3.resource('sqs')
    thumbnail_queue = sqs.get_queue_by_name(QueueName='getThumbnail')

    sql = "SELECT * FROM article WHERE thumbnail_url IS NULL OR thumbnail_url='test' OR thumbnail_url=''"
    articles = db.fetchAll(sql)
    msg_array = []
    for article in articles:
        msg_array.append(article['article_id'])

    r = thumbnail_queue.send_message(MessageBody=json.dumps(msg_array))
    logger.info(msg_array)
